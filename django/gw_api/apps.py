from django.apps import AppConfig


class GwApiConfig(AppConfig):
    name = 'gw_api'
