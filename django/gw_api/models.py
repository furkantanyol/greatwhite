from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField()
    content = models.TextField()
    author = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(
        Post, related_name='comments', on_delete=models.CASCADE)
    text = models.TextField()
    author = models.CharField(max_length=100)

    def __str__(self):
        return "[%s] %s" % (self.author, self.text)


class Slide(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField()
    synopsis = models.TextField()
    link = models.CharField(max_length=200)

    def __str__(self):
        return self.title
