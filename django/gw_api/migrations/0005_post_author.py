# Generated by Django 2.1.4 on 2019-06-26 09:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gw_api', '0004_post_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='author',
            field=models.CharField(default='', max_length=50),
            preserve_default=False,
        ),
    ]
