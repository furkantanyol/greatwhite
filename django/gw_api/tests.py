from django.test import TestCase
from .models import Post
import datetime


class PostModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Post.objects.create(title='first post')
        Post.objects.create(content='content here')
        Post.objects.create(created_at=datetime.datetime(2020, 5, 17))
        Post.objects.create(updated_at=None)

    def test_title(self):
        post = Post.objects.get(id=1)
        expected_object_name = f'{post.title}'
        self.assertEquals(expected_object_name, 'first post')

    def test_content(self):
        post = Post.objects.get(id=2)
        expected_object_name = f'{post.content}'
        self.assertEquals(expected_object_name, 'content here')

    def test_created_at(self):
        post = Post.objects.get(id=3)
        expected_object_name = f'{post.created_at}'
        self.assertEquals(expected_object_name, '2020-05-17 00:00:00')

    def test_updated_at(self):
        post = Post.objects.get(id=4)
        expected_object_name = f'{post.updated_at}'
        self.assertEquals(expected_object_name, None)
