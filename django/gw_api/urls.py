from django.urls import path

from . import views

urlpatterns = [
    # path('', views.PostList.as_view()),
    path('posts/', views.PostList.as_view()),
    path('posts/<int:pk>/', views.PostDetail.as_view()),
    path('comments/', views.CommentList.as_view()),
    path('comment/<int:pk>/', views.CommentDetail.as_view()),
    path('slides/', views.SlideList.as_view()),
    path('slides/<int:pk>/', views.SlideDetail.as_view()),
]
