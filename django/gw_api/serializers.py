from rest_framework import serializers
from .models import Post, Comment, Slide


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('text', 'author', 'post')


class PostSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = Post
        fields = ('id', 'title', 'image', 'content', 'author',
                  'created_at', 'updated_at', 'comments')


class SlideSerializer(serializers.ModelSerializer):
    class Meta:
        model = Slide
        fields = ('title', 'image', 'synopsis', 'link')
