from django.contrib import admin
from .models import Post, Comment, Slide

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Slide)
