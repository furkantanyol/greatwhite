require('dotenv').config()
const withCSS = require('@zeit/next-css')
const withFonts = require('next-fonts')
const withTypescript = require('@zeit/next-typescript')
const path = require('path')

const secrets = {
  serverRuntimeConfig: {
    REACT_APP_CMS: process.env.REACT_APP_CMS
  },
  publicRuntimeConfig: {
    REACT_APP_CMS: process.env.REACT_APP_CMS
  }
}

const webpackConfig = {
  target: 'serverless',
  webpack(config) {
    config.module.rules.push({
      test: /\.(jpe?g|png|gif|ttf|eot|woff|svg)$/i,
      loader: 'url-loader'
    })
    config.resolve.modules.push(path.resolve('./'))

    return config
  }
}

const nextConfig = {
  ...secrets,
  ...webpackConfig
}

module.exports = withCSS(withFonts(withTypescript(nextConfig)))
