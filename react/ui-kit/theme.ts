const theme = {
  heading: "'Lato', sans-serif",
  number: "'Lato', sans-serif",
  body: "'Lato', sans-serif",
  shadows: {
    low: '0 1px 2px rgba(0, 0, 0, 0.15)',
    lowHover: '0 1px 2px rgba(0, 0, 0, 0.3), 0 3px 6px rgba(0, 0, 0, 0.15)',
    medium: '0 2px 4px rgba(0, 0, 0, 0.2)',
    mediumHover: '0 2px 4px rgba(0, 0, 0, 0.4), 0 6px 12px rgba(0, 0, 0, 0.3)'
  },
  colors: {
    black: '#000',
    white: '#fff',
    // brand colours
    wedgewood: '#4f7cac', // blue
    seafarer: '#0077FF', // mid blue
    skyBlue: '#00CCFF', // sky blue
    darkBlue: '#1c1d21', // dark blue
    alto: '#d8d8d8', // light grey
    granite: '#9b9b9b', // mid grey
    charade: '#272932', // almost-black
    // ui colours
    lima: '#7ed321', // green
    monza: '#CC2D38', // red
    alizarin: '#e74c3c', // mid red
    pomegranate: '#c0392b', // dark red
    orange: '#F5A623', // orange
    // random colours
    regentgrey: '#7c8c97', // some light blue he uses on text
    porcelain: '#eaecee', // 0.16 opacity $regentgrey
    alabaster: '#f8f8f8', // almost-white
    // modals
    cornflowerblue: '#5f5fff',
    night: '#090054',
    day: '#f8f8fa'
  },
  transitions: {
    fast: '0.195s all cubic-bezier(0.4, 0.0, 0.2, 1)',
    slow: '0.375s all cubic-bezier(0.4, 0.0, 0.2, 1)',
    deceleratingSlow: '0.375s all cubic-bezier(0.0, 0.0, 0.2, 1)',
    deceleratingFast: '0.195s all cubic-bezier(0.0, 0.0, 0.2, 1)'
  },
}

export default theme
