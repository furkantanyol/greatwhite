import React from 'react'
import { BlogPostType } from 'types'
import Link from 'next/link'
import { createDate } from 'lib/helpers/createDate'
import './BlogPostCard.css'

type BlogPostCardViewProps = {
  blogPost: BlogPostType
}

const BlogPostCardView: React.FC<BlogPostCardViewProps> = ({ blogPost }) => (
  <Link as={`/blog/${blogPost.id}`} href={`/blogPost?id=${blogPost.id}`}>
    <div className="blog-post-card" key={blogPost.id}>
      <div className="blog-post-image-wrapper">
        <img
          className="blog-post-card-image"
          src={blogPost.image}
          alt={blogPost.title}
        />
      </div>

      <div className="blog-post-text-wrapper">
        <p className="blog-post-card-title">{blogPost.title}</p>
        <div className="blog-post-card-nameDate">
          <p className="blog-post-card-author">{blogPost.author}</p>
          <p className="blog-post-card-createdAt">
            {createDate(blogPost.created_at)}
          </p>
        </div>
      </div>
    </div>
  </Link>
)

export default BlogPostCardView
