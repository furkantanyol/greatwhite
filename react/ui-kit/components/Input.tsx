import React from 'react'

type InputProps = {
  name: string
  title: string
  type: string
  value: string
  onChange(value: string): void
  placeholder: string
}

export class Input extends React.Component<InputProps> {
  onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (this.props.onChange) {
      this.props.onChange(e.target.value)
    }
  }

  render() {
    const { name, title, type, value, placeholder } = this.props

    return (
      <div className="form-group">
        <label htmlFor={name} className="form-label">
          {title}
        </label>
        <input
          className="form-control"
          id={name}
          name={name}
          type={type}
          value={value}
          onChange={this.onChange}
          placeholder={placeholder}
        />
      </div>
    )
  }
}
