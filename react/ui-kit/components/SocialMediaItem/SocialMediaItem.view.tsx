import React from 'react'
import './SocialMediaItem.css'

type SocialMediaItemProps = {
  href: string
  imageUrl: string
}

const SocialMediaItemView: React.FC<SocialMediaItemProps> = ({
  href,
  imageUrl
}) => (
  <React.Fragment>
    <a target="_blank" rel="noopener noreferrer" href={href} className="link">
      <div
        style={{ backgroundImage: `url(${imageUrl})` }}
        className={'social-media-logo'}
      />
    </a>
  </React.Fragment>
)

export default SocialMediaItemView
