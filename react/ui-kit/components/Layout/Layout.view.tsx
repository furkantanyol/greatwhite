import Header from 'ui-kit/components/Header'
import Footer from 'ui-kit/components/Footer'
import './Layout.css'
import React from 'react'

const LayoutView: React.FC<{ children: React.ReactNode }> = ({ children }) => (
  <div className="layout">
    <Header />
    <div className="main-content">{children}</div>
    <Footer />
  </div>
)
export default LayoutView
