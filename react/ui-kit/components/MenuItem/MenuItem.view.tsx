import React from 'react'
import Link from 'next/link'
import './MenuItem.css'

type MenuItemProps = {
  name: string
  href: string
}

const MenuItemView: React.FC<MenuItemProps> = ({ name, href }) => (
  <React.Fragment>
    <Link prefetch href={href}>
      <a className="link">{name}</a>
    </Link>
  </React.Fragment>
)

export default MenuItemView
