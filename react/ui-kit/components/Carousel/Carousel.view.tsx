import React from 'react'
import SlideContainer from './SlideContainer'
import DotNavigation from './DotNavigation'
import ArrowButton from './ArrowButton'
import './Carousel.css'

type CarouselViewProps = {
  onPreviousClick: () => void
  onNextClick: () => void
  slides: object[]
  slideIndex: number
  numberOfSlides: number
  goToSlide: (i: number) => void
}

const CarouselView: React.FC<CarouselViewProps> = ({
  onPreviousClick,
  onNextClick,
  slides,
  slideIndex,
  numberOfSlides,
  goToSlide
}) => (
  <React.Fragment>
    <div className="root">
      <ArrowButton direction="left" onClick={onPreviousClick} />
      <div className="slideshow-container">
        <React.Fragment>
          {slides.map((slide: any, i: number) => (
            <SlideContainer
              slide={slide}
              key={'slide-' + i}
              active={i + 1 === slideIndex}
              visible={i + 1 === slideIndex}
            />
          ))}
        </React.Fragment>
        <DotNavigation
          numberOfSlides={numberOfSlides}
          goToSlide={goToSlide}
          slideIndex={slideIndex}
        />
      </div>
      <ArrowButton direction="right" onClick={onNextClick} />
    </div>
  </React.Fragment>
)

export default CarouselView
