import React from 'react'
import './DotNavigation.css'

type DotNavigationViewProps = { dots: React.ReactNode }

const DotNavigationView: React.FC<DotNavigationViewProps> = ({ dots }) => (
  <div className="dots">{dots}</div>
)

export default DotNavigationView
