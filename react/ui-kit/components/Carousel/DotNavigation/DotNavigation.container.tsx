import React, { Component } from 'react'
import CarouselDot from 'ui-kit/components/Carousel/CarouselDot'
import DotNavigationView from './DotNavigation.view'

type DotNavigationContainerProps = {
  numberOfSlides: number
  goToSlide: (i: number) => void
  slideIndex: number
}

class DotNavigationContainer extends Component<DotNavigationContainerProps> {
  state = {
    dots: []
  }

  componentDidMount() {
    const { numberOfSlides } = this.props
    this.updateDots(numberOfSlides)
  }

  componentDidUpdate(prevProps: { slideIndex: number }) {
    const { numberOfSlides, slideIndex } = this.props
    if (prevProps.slideIndex !== slideIndex) this.updateDots(numberOfSlides)
  }

  updateDots(numberOfSlides: number) {
    const { slideIndex } = this.props
    let dots = []
    let i = 1

    while (i <= numberOfSlides) {
      dots.push(
        <CarouselDot
          index={i}
          active={i === slideIndex ? true : false}
          dotOnClick={this.handleDotClick}
        />
      )
      i++
    }
    this.setState({ dots })
  }

  handleDotClick = (i: number) => {
    this.props.goToSlide(i)
  }

  render() {
    const { dots } = this.state
    return <DotNavigationView dots={dots} />
  }
}

export default DotNavigationContainer
