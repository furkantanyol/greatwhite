import React from 'react'
import CarouselView from './Carousel.view'

type CarouselContainerProps = {
  slides: object[]
}

class CarouselContainer extends React.Component<CarouselContainerProps> {
  state = {
    slideIndex: 1
  }

  goToSlide = (i: number) => {
    this.setState({
      slideIndex: i
    })
  }

  goNext = () => {
    const { slideIndex } = this.state
    const { slides } = this.props
    if (slideIndex === slides.length) {
      this.setState({
        slideIndex: 1
      })
    } else {
      this.setState((prevState: { slideIndex: number }) => ({
        slideIndex: prevState.slideIndex + 1
      }))
    }
  }

  goPrevious = () => {
    const { slideIndex } = this.state
    const { slides } = this.props
    if (slideIndex === 1) {
      this.setState({
        slideIndex: slides.length
      })
    } else {
      this.setState((prevState: { slideIndex: number }) => ({
        slideIndex: prevState.slideIndex - 1
      }))
    }
  }

  render() {
    const { slideIndex } = this.state
    const { slides } = this.props
    return (
      <CarouselView
        onPreviousClick={this.goPrevious}
        onNextClick={this.goNext}
        slides={slides}
        slideIndex={slideIndex}
        numberOfSlides={slides.length}
        goToSlide={this.goToSlide}
      />
    )
  }
}

export default CarouselContainer
