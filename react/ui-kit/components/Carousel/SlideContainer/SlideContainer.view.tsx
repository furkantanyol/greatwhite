import React from 'react'
import Button from 'ui-kit/components/Button'
import './SlideContainer.css'

type SlideContainerProps = {
  slide: {
    image: string
    title: string
    synopsis: string
    link: string
  }
  active: boolean
  visible: boolean
}

const SlideContainer: React.FC<SlideContainerProps> = ({
  slide,
  active,
  visible
}) => (
  <div
    className={`slide-container ${active ? 'slide-container-active' : ''} ${
      visible ? 'slide-container-visible' : ''
    }`}
  >
    <img className="slide-image" src={slide.image} alt={slide.title} />
    <h4 className="slide-title">{slide.title.toUpperCase()}</h4>
    <p className="slide-synopsis">{slide.synopsis}</p>
    <Button href={slide.link}>GO TO WEBSITE</Button>
  </div>
)

export default SlideContainer
