import React from 'react'
import PropTypes from 'prop-types'
import './ArrowButton.css'

type ArrowButtonViewProps = {
  direction: string
  onClick: () => void
}

const ArrowButtonView: React.FC<ArrowButtonViewProps> = ({
  direction,
  onClick
}) => (
  <button className="arrow-button" onClick={onClick}>
    {direction === 'left' ? <p>&#10094;</p> : <p>&#10095;</p>}
  </button>
)

ArrowButtonView.propTypes = {
  direction: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired
}

export default ArrowButtonView
