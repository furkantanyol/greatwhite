import React from 'react'
import './CarouselDot.css'

type CarouselDotViewProps = {
  active: boolean
  index: number
  dotOnClick: (index: number) => void
}

const CarouselDotView: React.FC<CarouselDotViewProps> = ({
  active,
  index,
  dotOnClick
}) => (
  <React.Fragment>
    <span
      className={active ? 'dot dot-active' : 'dot'}
      key={'dots-' + index}
      role="button"
      tabIndex={0}
      onClick={() => {
        dotOnClick(index)
      }}
    />
  </React.Fragment>
)

export default CarouselDotView
