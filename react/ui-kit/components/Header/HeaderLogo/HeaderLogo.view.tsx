import React from 'react'
import PropTypes from 'prop-types'
import LogoHome from 'ui-kit/images/logo-home.svg'
import LogoAway from 'ui-kit/images/logo-away.svg'
import './HeaderLogo.css'

const HeaderLogoView: React.FC<{ scrolled: boolean }> = ({ scrolled }) => {
  return (
    <React.Fragment>
      <img
        className={`logo ${scrolled ? 'logo-active' : ''}`}
        src={LogoAway}
        alt="logo-home"
      />
      <img
        className={`logo ${scrolled ? '' : 'logo-active'}`}
        src={LogoHome}
        alt="logo-away"
      />
    </React.Fragment>
  )
}

HeaderLogoView.propTypes = {
  scrolled: PropTypes.bool.isRequired
}

export default HeaderLogoView
