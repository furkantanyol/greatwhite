import React, { useCallback, useEffect, useState } from 'react'
import { paths } from 'lib/helpers/paths'
import MenuItem from 'ui-kit/components/MenuItem'
import HeaderLogo from 'ui-kit/components/Header/HeaderLogo'
import './Header.css'
import throttle from 'lodash.throttle'

// todo move these two custom hooks to their own file
// todo find out what type the any should be - idk what the ref is supposed to be
function useClientRect(): [DOMRect | null, any] {
  const [rect, setRect] = useState(null)
  const ref = useCallback(node => {
    if (node !== null) {
      setRect(node.getBoundingClientRect())
    }
  }, [])
  return [rect, ref]
}

function useWindowScroll(): number {
  const [position, setPosition] = useState(0)

  const updatePosition = throttle(() => {
    setPosition(window.scrollY)
  }, 100)

  useEffect(() => {
    window.addEventListener('scroll', updatePosition)
    return () => window.removeEventListener('scroll', updatePosition)
  }, [])

  return position
}

type HeaderProps = {
  children?: React.ReactNode
}

type DefaultHeaderContentProps = {
  paths: any
  showOpaqueHeader: boolean
}

const DefaultHeaderContent = ({
  paths,
  showOpaqueHeader
}: DefaultHeaderContentProps) => (
  <div className="header-content">
    <div className="logo-container">
      <HeaderLogo scrolled={showOpaqueHeader} />
    </div>
    <div
      className={`links-container ${
        showOpaqueHeader ? 'links-container-active' : ''
      }`}
    >
      <MenuItem href={paths.home} name="HOME" />
      <MenuItem href={paths.blog} name="BLOG" />
    </div>
  </div>
)

const HeaderView: React.FC<HeaderProps> = ({ children }) => {
  // todo remove the showOpaqueHeader prop on the default header and move this code to that component
  const [rect, ref] = useClientRect()
  const scrollPosition = useWindowScroll()
  const height = rect ? rect.height : 0
  const showOpaqueHeader = scrollPosition > height

  return (
    <header
      ref={ref}
      className={`header-wrapper ${showOpaqueHeader ? 'header-active' : ''}`}
    >
      {children && children}
      {!children && (
        <DefaultHeaderContent
          paths={paths}
          showOpaqueHeader={showOpaqueHeader}
        />
      )}
    </header>
  )
}

export default HeaderView
