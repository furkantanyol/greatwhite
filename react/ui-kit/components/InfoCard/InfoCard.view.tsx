import React from 'react'
import './InfoCard.css'

type InfoCardProps = {
  image: string
  text: string
  title: string
}

const InfoCardView: React.FC<InfoCardProps> = ({ image, text, title }) => (
  <div className="info-card">
    <img className="info-card-image" src={image} alt="idev" />
    <p className="info-card-title">{title}</p>
    <p className="info-card-text">{text}</p>
  </div>
)

export default InfoCardView
