import React from 'react'

type TextAreaProps = {
  name: string
  title: string
  value: string
  cols?: number
  placeholder?: string
  rows?: number
  onChange?(value: string): void
}

export class TextArea extends React.Component<TextAreaProps> {
  onChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    if (this.props.onChange) {
      this.props.onChange(e.target.value)
    }
  }

  render() {
    const { title, name, rows = 4, cols = 42, value, placeholder } = this.props

    return (
      <div className="form-group">
        <label className="form-label">{title}</label>
        <textarea
          className="form-control"
          name={name}
          rows={rows}
          cols={cols}
          value={value}
          onChange={this.onChange}
          placeholder={placeholder}
        />
      </div>
    )
  }
}
