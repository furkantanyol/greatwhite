import React from 'react'
import { BlogPostType } from 'types'
import './Comments.css'

type CommentsViewProps = {
  blogPost: BlogPostType
}
const CommentsView: React.FC<CommentsViewProps> = ({ blogPost }) => {
  if (!blogPost.comments || blogPost.comments.length === 0) return null

  return (
    <div className="comments-container">
      {blogPost.comments.map(comment => (
        <div className="comment" key={comment.text}>
          <h2 className="comment-author">{comment.author}</h2>
          <p className="comment-text">{comment.text}</p>
        </div>
      ))}
    </div>
  )
}

export default CommentsView
