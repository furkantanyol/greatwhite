import React from 'react'
import './IntroSection.css'

type IntroSectionProps = {
  image: string
  children?: React.ReactNode
}
const IntroSectionView: React.FC<IntroSectionProps> = ({ image, children }) => (
  <div className="home-wrapper">
    <img className="home-image" src={image} alt="intro-image" />
    {children}
  </div>
)

export default IntroSectionView
