import React from 'react'
import { ButtonType } from 'types'
import './Button.css'

const ButtonView: React.FC<ButtonType | any> = ({
  className,
  href,
  onClick,
  children
}) => {
  if (href) {
    return (
      <a
        className={`${className} button-wrapper`}
        href={href}
        target="_blank"
        rel="noopener noreferrer"
      >
        {children}
      </a>
    )
  }

  return (
    <button className={`${className} button-wrapper`} onClick={onClick}>
      {children}
    </button>
  )
}

export default ButtonView
