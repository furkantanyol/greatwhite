import React from 'react'
import { Field } from 'formik'
import './FormField.css'

type FormFieldProps = {
  placeholder: string
  name: string
  type: string
  label: string
  error: string | undefined
  component: string
}

const FormField: React.FC<FormFieldProps> = ({
  placeholder,
  name,
  type,
  error,
  component,
  label
}) => (
  <div className="form-field-wrapper">
    <label htmlFor={name} className="form-label">
      {label}
    </label>
    <Field
      placeholder={placeholder}
      name={name}
      type={type}
      component={component}
      className={`form-input ${error ? ' is-invalid' : ''}`}
    />
    <span className={`validation-error-label ${error ? '' : 'invisible'}`}>
      {error}
    </span>
  </div>
)

export default FormField
