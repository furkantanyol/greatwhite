import React from 'react'
import CommentFormView from './CommentForm.view'
import { SubmitCommentType } from 'types'

type CommentFormContainerProps = {
  handleSubmit: SubmitCommentType
}

class CommentFormContainer extends React.Component<CommentFormContainerProps> {
  render() {
    const { handleSubmit } = this.props
    return <CommentFormView handleSubmit={handleSubmit} />
  }
}

export default CommentFormContainer
