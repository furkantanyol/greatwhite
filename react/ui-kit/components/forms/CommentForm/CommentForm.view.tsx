import React from 'react'
import Button from 'ui-kit/components/Button'
import { SubmitCommentType } from 'types'
import { Formik, Form } from 'formik'
import * as Yup from 'yup'
import FormField from 'ui-kit/components/forms/FormField'
import FormStatus from 'ui-kit/components/forms/FormStatus'
import './CommentForm.css'

type CommentFormViewProps = {
  handleSubmit: SubmitCommentType | any
}

const CommentFormView: React.FC<CommentFormViewProps> = ({ handleSubmit }) => (
  <Formik
    initialValues={{
      author: '',
      comment: ''
    }}
    validationSchema={Yup.object().shape({
      author: Yup.string().required('Your name is required'), // add necessary validations here
      comment: Yup.string().required('Your comment is required')
    })}
    onSubmit={handleSubmit}
    render={({ errors, status, isSubmitting }) => (
      <Form className="comment-form">
        <FormField
          placeholder={'Enter name'}
          component={'input'}
          name={'author'}
          type={'text'}
          error={errors.author}
          label={'Name'}
        />
        <FormField
          placeholder={'Enter comment'}
          name={'comment'}
          type={'comment'}
          component={'textarea'}
          error={errors.comment}
          label={'Comment'}
        />
        <Button className="comment-submit-button" onClick={handleSubmit}>
          Submit
        </Button>
        <FormStatus status={status} />
      </Form>
    )}
  />
)

export default CommentFormView
