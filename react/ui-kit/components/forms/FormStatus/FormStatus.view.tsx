import React from 'react'

const FormStatusView: React.FC<{ status: string }> = ({ status }) => (
  <div className={`alert ${status ? '' : 'invisible'}`}>{status}</div>
)

export default FormStatusView
