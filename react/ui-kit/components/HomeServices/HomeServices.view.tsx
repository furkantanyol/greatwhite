import React from 'react'
import './HomeServices.css'
import InfoCard from 'ui-kit/components/InfoCard'
import iconDev from 'ui-kit/images/icon-dev.svg'
import iconBranding from 'ui-kit/images/icon-branding.svg'
import iconDesign from 'ui-kit/images/icon-design.svg'
import iconIllustration from 'ui-kit/images/icon-illustration.svg'

const HomeServicesView: React.FC = () => (
  <div className="services-container">
    <p className="services-title">What We Do</p>
    <div className="services-wrapper">
      <InfoCard
        image={iconDev}
        title="Development"
        text="JavaScript/ES6, HTML5, CSS3(SASS), Web App with React, Redux/Mobx, WordPress , Design to Web UI, Python/Django CMS"
      />
      <InfoCard
        image={iconIllustration}
        title="Illustration / Icon"
        text="Character Design, Icon Set, Illustration Guide, Illustration Set"
      />
      <InfoCard
        image={iconDesign}
        title="Design"
        text="Web App Design, Landing Pages, User Flow, Wireframing, Prototyping, Mobile App Design"
      />
      <InfoCard
        image={iconBranding}
        title="Branding"
        text="Visual Identity, Stationary Kit, Marketing Materials"
      />
    </div>
  </div>
)

export default HomeServicesView
