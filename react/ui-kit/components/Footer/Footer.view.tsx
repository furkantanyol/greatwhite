import React from 'react'
import { paths } from 'lib/helpers/paths'
import SocialMediaItem from 'ui-kit/components/SocialMediaItem'
import facebook from 'ui-kit/images/facebook.svg'
import instagram from 'ui-kit/images/instagram.svg'
import twitter from 'ui-kit/images/twitter.svg'
import Button from 'ui-kit/components/Button'
import abstractImage from 'ui-kit/images/ft-abstract-1.svg'
import './Footer.css'

type FooterViewProps = {
  children?: React.ReactNode
}

const FooterView: React.FC<FooterViewProps> = ({ children }) => {
  const defaultFooterContent = (
    <React.Fragment>
      <div
        className="reach-wrapper"
        style={{ backgroundImage: `url(${abstractImage})` }}
      >
        <p className="reach-text">Interested to work with us?</p>
        <Button className="reach-button" href={`mailto:furkantanyol@gmail.com`}>
          LET&apos;S TALK
        </Button>
      </div>

      <div className="social-media-container">
        <SocialMediaItem imageUrl={facebook} href={paths.facebook} />
        <SocialMediaItem imageUrl={instagram} href={paths.instagram} />
        <SocialMediaItem imageUrl={twitter} href={paths.twitter} />
      </div>
    </React.Fragment>
  )
  return (
    <footer className="footer-wrapper">
      {children ? children : defaultFooterContent}
    </footer>
  )
}

export default FooterView
