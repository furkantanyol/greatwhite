import React from 'react'
import { storiesOf } from '@storybook/react'
import Button from 'ui-kit/components/Button'
import { action } from '@storybook/addon-actions'

storiesOf('Button', module)
  .add('with text', () => (
    <Button onClick={action('button-click')}>SIGN UP</Button>
  ))
  .add('with emoji', () => (
    <Button onClick={action('button-click')}>
      <span role="img" aria-label="so cool">
        😀 😎 👍 💯
      </span>
    </Button>
  ))
