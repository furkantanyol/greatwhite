import React from 'react'
import { storiesOf } from '@storybook/react'
import Header from 'ui-kit/components/Header'
import Link from 'next/link'
import { paths } from 'lib/helpers/paths'

storiesOf('Header', module)
  .add('default', () => <Header />)
  .add('with children', () => (
    <Header>
      <Link prefetch href={paths.blog}>
        <a>Blog</a>
      </Link>
    </Header>
  ))
