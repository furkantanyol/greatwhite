import React from 'react'
import Footer from 'ui-kit/components/Footer'
import Button from 'ui-kit/components/Button'
import { storiesOf } from '@storybook/react'

storiesOf('Footer', module)
  .add('default', () => <Footer />)
  .add('with children', () => (
    <Footer>
      <Button href={`mailto:furkantanyol@gmail.com`}>Lets Talk</Button>
    </Footer>
  ))
