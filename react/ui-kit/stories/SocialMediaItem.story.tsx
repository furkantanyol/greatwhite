import React from 'react'
import { storiesOf } from '@storybook/react'
import SocialMediaItem from 'ui-kit/components/SocialMediaItem'
import { paths } from 'lib/helpers/paths'
import instagram from 'ui-kit/images/instagram.svg'
import twitter from 'ui-kit/images/twitter.svg'
import facebook from 'ui-kit/images/facebook.svg'

storiesOf('SocialMediaItem', module)
  .add('default', () => (
    <SocialMediaItem imageUrl={facebook} href={paths.facebook} />
  ))
  .add('a couple', () => (
    <div style={{ display: 'flex' }}>
      <SocialMediaItem imageUrl={facebook} href={paths.facebook} />
      <SocialMediaItem imageUrl={instagram} href={paths.instagram} />
      <SocialMediaItem imageUrl={twitter} href={paths.twitter} />
    </div>
  ))
