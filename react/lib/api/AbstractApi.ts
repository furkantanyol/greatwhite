export default class AbstractApi {
  baseUrl: string | undefined
  constructor(baseUrl: string | undefined) {
    if (new.target === AbstractApi)
      throw new TypeError('Cannot construct abstract class directly')

    if (!baseUrl) throw new Error('baseUrl is not set')

    let url = baseUrl.trim()
    if (!url.endsWith('/')) {
      url = url + '/'
    }

    this.baseUrl = url
  }

  buildAbsoluteUrl = (url: string): string => {
    return this.baseUrl + url
  }

  buildAuthHeader(token: string): object {
    return {
      Authorization: 'Bearer ' + token
    }
  }
}
