import AbstractApi from './AbstractApi'
import fetch from 'isomorphic-unfetch'
import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

class SlideApi extends AbstractApi {
  constructor(baseUrl: string | undefined) {
    super(baseUrl)

    this.baseUrl = baseUrl
  }

  getSlides = async () => {
    const url = `${this.baseUrl}/slides/`
    const res = await fetch(url)
    const slides = await res.json()
    return slides
  }
}

const api = new SlideApi(
  serverRuntimeConfig.REACT_APP_CMS || publicRuntimeConfig.REACT_APP_CMS
)
export default api
