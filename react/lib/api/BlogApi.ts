import AbstractApi from './AbstractApi'
import fetch from 'isomorphic-unfetch'
import getConfig from 'next/config'
const { serverRuntimeConfig, publicRuntimeConfig } = getConfig()

class BlogApi extends AbstractApi {
  constructor(baseUrl: string | undefined) {
    super(baseUrl)

    this.baseUrl = baseUrl
  }

  getBlogPosts = async () => {
    const res = await fetch(`${this.baseUrl}/posts`)
    const blogPosts = await res.json()
    return blogPosts
  }

  getBlogPost = async (id: string): Promise<object> => {
    const res = await fetch(`${this.baseUrl}/posts/${id}`)
    const blogPost = await res.json()
    return blogPost
  }

  submitComment = async (id: string, author: string, text: string) => {
    const options = {
      method: 'post',
      headers: {
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        post: id,
        author,
        text
      })
    }
    const res = await fetch(`${this.baseUrl}/comments/`, options)
    return res
  }
}

const api = new BlogApi(
  serverRuntimeConfig.REACT_APP_CMS || publicRuntimeConfig.REACT_APP_CMS
)
export default api
