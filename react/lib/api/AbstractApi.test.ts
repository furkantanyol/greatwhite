import AbstractApi from './AbstractApi'

class TestApi extends AbstractApi {
  constructor(baseUrl: string | undefined) {
    super(baseUrl)
  }
}

describe('AbstractApi tests', () => {
  it('should not allow to create instances of AbstractApi', () => {
    expect(() => new AbstractApi('https://url')).toThrow(TypeError)
  })

  it('should leave last forward slash from base url', () => {
    const api = new TestApi('http://example.com/')
    expect(api.baseUrl).toEqual('http://example.com/')
  })

  it('should add last forward slash to base url', () => {
    const api = new TestApi('http://foo.bar')
    expect(api.baseUrl).toEqual('http://foo.bar/')
  })
})
