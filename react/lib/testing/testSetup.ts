import 'jest-dom/extend-expect'
import 'react-testing-library/cleanup-after-each'
import registerRequireContextHook from 'babel-plugin-require-context-hook/register'
registerRequireContextHook()
