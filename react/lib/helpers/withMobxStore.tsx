import React from 'react'
import initializeStore from 'stores/index'
import { AppComponentContext } from 'next/app'
const isServer = typeof window === 'undefined'
const __NEXT_MOBX_STORE__: any = '__NEXT_MOBX_STORE__'

const getOrCreateStore = (initialState?: any) => {
  // Always make a new store if server, otherwise state is shared between requests
  if (isServer) {
    return initializeStore(initialState)
  }

  // Create store if unavailable on the client and set it on the window object
  if (!window[__NEXT_MOBX_STORE__]) {
    window[__NEXT_MOBX_STORE__] = initializeStore(initialState)
  }
  return window[__NEXT_MOBX_STORE__]
}

type NextAppTypes = {
  initialMobxState: any
}

export default (App: any) => {
  return class NextApp extends React.Component<NextAppTypes> {
    static async getInitialProps(appContext: AppComponentContext | any) {
      const mobxStore = getOrCreateStore()
      appContext.ctx.mobxStore = mobxStore

      let appProps = {}
      if (typeof App.getInitialProps === 'function') {
        appProps = await App.getInitialProps.call(App, appContext)
      }

      return {
        ...appProps,
        initialMobxState: mobxStore
      }
    }

    mobxStore = getOrCreateStore(this.props.initialMobxState)

    render() {
      return <App {...this.props} mobxStore={this.mobxStore} />
    }
  }
}
