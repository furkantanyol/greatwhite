import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import customParseFormat from 'dayjs/plugin/customParseFormat'
dayjs.extend(utc)
dayjs.extend(customParseFormat)

const createDate = (date: string) => dayjs.utc(date).format('DD MMM, YYYY')

export { createDate }
