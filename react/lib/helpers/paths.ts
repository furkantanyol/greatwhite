const paths = {
  facebook: 'https://facebook.com',
  instagram: 'https://instagram.com',
  twitter: 'https://twitter.com',
  home: '/',
  blog: '/blog'
}

const innerPaths = {
  home: '',
  blog: 'blog'
}
export { paths, innerPaths }
