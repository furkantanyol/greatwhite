const express = require('express')
const next = require('next')
const cors = require('cors')
const mobxReact = require('mobx-react')
const dotenv = require('dotenv')
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

dotenv.config()
mobxReact.useStaticRendering(true)

app
  .prepare()
  .then(() => {
    const server = express()

    server.use(cors())

    server.get('/blog/:id', (req, res) => {
      const params = { id: req.params.id }
      return app.render(req, res, '/blog', params)
    })

    server.get('*', (req, res) => {
      return handle(req, res)
    })

    server.listen(3000, err => {
      if (err) throw err
      // console.log('> Ready on http://localhost:3000')
    })
  })
  .catch(() => {
    //ex
    // console.error(ex.stack)
    process.exit(1)
  })
