import { configure } from '@storybook/react'
import '../lib/testing/mockRouter'

if (process.env.NODE_ENV === 'test') {
  require('babel-plugin-require-context-hook/register')()
}
const loadStories = () => {
  const req = require.context('../ui-kit/stories', true, /\.story\.js$/)
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
