import 'react'
declare module 'react' {
  interface StyleHTMLAttributes<T> extends React.HTMLAttributes<T> {
    jsx?: boolean
    global?: boolean
  }
}
declare module 'babel-plugin-require-context-hook/register'
declare module '*.tsx'
declare module '*.ts'
declare module '*.jpeg'
declare module '*.svg'
declare module '*.png'
