import { FormEvent, ChangeEvent } from 'react'

export type BlogPostType = {
  id: string
  title: string
  image: string
  content: string
  author: string
  created_at: string
  comments?: CommentType[]
}

export type SubmitCommentType = ({
  author,
  comment
}: {
  author: string
  comment: string
}) => Promise<void>

export type FormEventType = FormEvent<HTMLFormElement>
export type ChangeEventType =
  | ChangeEvent<HTMLInputElement>
  | ChangeEvent<HTMLTextAreaElement>
export type HandleInputType = (event: FormEventType) => void
export type HandleChangeType = (event: ChangeEventType) => void

export type BlogType = {}

export type BlogStoreType = {
  blogPost: BlogPostType
  blogPosts: BlogPostType[]
  fetchBlogPosts: () => Promise<BlogPostType[]>
  fetchBlogPost: (id: string) => Promise<BlogPostType>
}

export type MobxStoreType = {
  blogStore: BlogStoreType
}

export type CommentType = {
  author: string
  text: string
}

export type ButtonType = {
  className: string
  href?: string
  onClick?: () => void
  children: React.ReactNode
}
