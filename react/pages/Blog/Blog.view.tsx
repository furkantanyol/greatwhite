import React from 'react'
import Layout from 'ui-kit/components/Layout'
import { BlogPostType } from 'types'
import IntroSection from 'ui-kit/components/IntroSection'
import BlogPostCard from 'ui-kit/components/BlogPostCard'
import blogImage from 'ui-kit/images/blog-background.svg'
import { toJS } from 'mobx'
import './Blog.css'

type BlogViewProps = {
  blogPosts: BlogPostType[]
}
const BlogView: React.FC<BlogViewProps> = ({ blogPosts }) => (
  <Layout>
    <IntroSection image={blogImage} />
    <div className="blog-post-wrapper">
      {blogPosts.map(blogPost => (
        <BlogPostCard key={blogPost.id} blogPost={toJS(blogPost)} />
      ))}
    </div>
  </Layout>
)

export default BlogView
