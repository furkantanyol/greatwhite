import React, { Component } from 'react'
import BlogView from './Blog.view'
import { observer, inject } from 'mobx-react'

import { MobxStoreType } from 'types'

type BlogContainerProps = {
  mobxStore: MobxStoreType
}

@inject('mobxStore')
@observer
class BlogContainer extends Component<BlogContainerProps> {
  static async getInitialProps({ mobxStore }: BlogContainerProps) {
    if (mobxStore) {
      await mobxStore.blogStore.fetchBlogPosts()
      return { blogPosts: mobxStore.blogStore.blogPosts }
    }
    return { blogPosts: [] }
  }

  render() {
    const { blogPosts } = this.props.mobxStore.blogStore || []
    return <BlogView blogPosts={blogPosts} />
  }
}

export default BlogContainer
