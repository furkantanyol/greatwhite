import React from 'react'
import Layout from 'ui-kit/components/Layout'
import ReactMarkdown from 'react-markdown'
import CommentForm from 'ui-kit/components/forms/CommentForm'
import Comments from 'ui-kit/components/Comments'
import { BlogPostType, SubmitCommentType } from 'types'
import './BlogPost.css'

const BlogPostView: React.FC<{
  blogPost: BlogPostType
  handleSubmit: SubmitCommentType
}> = ({ blogPost, handleSubmit }) => {
  return (
    <Layout>
      <div className="blog-post-main-content">
        <div className="blog-post-container">
          <h1 className="blog-post-title">{blogPost.title}</h1>
          <img
            className="blog-post-image"
            src={blogPost.image}
            alt={blogPost.id}
          />
          <div className="markdown-wrapper">
            <ReactMarkdown source={blogPost.content} />
          </div>
        </div>
        <div className="comments-wrapper">
          <div className="comment-form-container">
            <CommentForm handleSubmit={handleSubmit} />
          </div>
          <Comments blogPost={blogPost} />
        </div>
      </div>
    </Layout>
  )
}

export default BlogPostView
