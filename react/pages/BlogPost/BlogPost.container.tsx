import React, { Component } from 'react'
import BlogPostView from './BlogPost.view'
import BlogApi from 'lib/api/BlogApi'
import { observer, inject } from 'mobx-react'
import { BlogPostType, MobxStoreType, CommentType } from 'types'

type BlogPostProps = {
  blogPost: BlogPostType
  mobxStore: MobxStoreType
  query: {
    id: string
  }
}

@inject('mobxStore')
@observer
class BlogPostContainer extends Component<BlogPostProps> {
  static async getInitialProps({ mobxStore, query }: BlogPostProps) {
    await mobxStore.blogStore.fetchBlogPost(query.id)
    return { blogPost: mobxStore.blogStore.blogPost }
  }

  handleCommentSubmit = async ({
    author,
    comment
  }: {
    author: string
    comment: string
  }) =>
    // { setStatus, setSubmitting } : {setStatus:()=>void, setSubmitting:()=>void}
    {
      const { blogPost, mobxStore } = this.props
      const { status } = await BlogApi.submitComment(
        blogPost.id,
        author,
        comment
      )
      if (status === 200 || status === 201) {
        await mobxStore.blogStore.fetchBlogPost(blogPost.id)
      }
      // implement loading and stuff
    }

  render() {
    const { blogPost } = this.props.mobxStore.blogStore
    return (
      <BlogPostView
        blogPost={blogPost}
        handleSubmit={this.handleCommentSubmit}
      />
    )
  }
}

export default BlogPostContainer
