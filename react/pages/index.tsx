import React from 'react'
import Layout from 'ui-kit/components/Layout'
import IntroSection from 'ui-kit/components/IntroSection'
import HomeServices from 'ui-kit/components/HomeServices'
import Button from 'ui-kit/components/Button'
import homeImage from 'ui-kit/images/home-background.svg'

const App: React.FC = () => (
  <Layout>
    <IntroSection image={homeImage}>
      <Button
        className="reach-button-home"
        href={`mailto:furkantanyol@gmail.com`}
      >
        LET&apos;S TALK
      </Button>
    </IntroSection>
    <HomeServices />
  </Layout>
)

export default App
