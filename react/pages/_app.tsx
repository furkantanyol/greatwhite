import App, { AppComponentType, Container } from 'next/app'
import React from 'react'
import withMobxStore from 'lib/helpers/withMobxStore'
import { Provider } from 'mobx-react'
import Head from 'next/head'
import './globalStyles.css'

type NextAppProps = {
  mobxStore: object
  Component: AppComponentType
}

class NextApp extends App<NextAppProps> {
  render() {
    const { Component, pageProps, mobxStore } = this.props
    return (
      <Container>
        <Head>
          <title>Space Ocean</title>
        </Head>
        <Provider mobxStore={mobxStore}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    )
  }
}

export default withMobxStore(NextApp)
