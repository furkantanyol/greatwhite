import BlogApi from 'lib/api/BlogApi'
import { observable, action, runInAction } from 'mobx'

export type BlogPost = {
  id: string
  title: string
}

class BlogStore {
  @observable blogPost = {}
  @observable blogPosts = []

  constructor(initialData = { blogPost: {}, blogPosts: [] }) {
    this.blogPost = initialData.blogPost
    this.blogPosts = initialData.blogPosts
  }

  async fetchBlogPost(id: string) {
    const blogPost = await BlogApi.getBlogPost(id)
    runInAction(() => {
      this.setBlogPost(blogPost)
    })
  }

  async fetchBlogPosts() {
    const blogPosts = await BlogApi.getBlogPosts()
    runInAction(() => {
      this.setBlogPosts(blogPosts)
    })
  }

  @action setBlogPost(blogPost: {}) {
    this.blogPost = blogPost
  }

  @action setBlogPosts(blogPosts: []) {
    this.blogPosts = blogPosts
  }
}

export default BlogStore
