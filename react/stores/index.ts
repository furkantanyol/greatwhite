import { useStaticRendering } from 'mobx-react'

import BlogStore from './BlogStore'

const isServer = typeof window === 'undefined'
useStaticRendering(isServer)

let store: any = null

export default function initializeStore(initialStoreConfig: any = {}) {
  if (isServer) {
    return {
      blogStore: new BlogStore(initialStoreConfig.blogStore)
    }
  }
  if (store === null) {
    store = {
      blogStore: new BlogStore(initialStoreConfig.blogStore)
    }
  }

  return store
}
